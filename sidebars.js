module.exports = {
  sidebar: {
    BHG: [
      {
        Components: [
          'index',
          {
            Core: ['core/cmp-image'],
          }
        ],
      },
    ],
    Corales: [
      {
        Components: ['core/cmp-image'],
      },
    ],
    Demo: ['demo'],
  },
};